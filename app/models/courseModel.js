const mongoose = require("mongoose");

const Schema= mongoose.Schema;

const courseSchema= new Schema({
    __id: mongoose.Types.ObjectId,
    courseCode:
    {
        type:String,
        required:true
    },
    courseName: {
       type:String,
       required:true
    },
    price:{
        type:Number,
        required:true
    },
    discountPrice: {
        type:Number,
        required:true
    },
    duration: {
        type:String,
        required:true
    },
    level: {
        type:String,
        required:true
    },
    coverImage:{
        type:String,
        required: true
    },
    teacherName:{
        type:String,
        required:true
    },
    teacherPhoto:{
        type:String,
        required:true
    },
    isPopular:{
        type:Boolean,
        required:true
    },
    isTrending:{
        type:Boolean,
        required:true
    }
},
{
    timestamps:true
}
);

module.exports=mongoose.model("course",courseSchema);