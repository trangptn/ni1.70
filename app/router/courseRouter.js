const express=require('express');
const router=express.Router();

const path=require('path');

router.use(express.static(__dirname+'/../views'));

const{
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}=require("../controllers/courseController");

router.get("/",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname+ '/../views/sample.06restAPI.Course.v2.0.html'));
})
router.post("/devcamp-Course365/courses",createCourse);
router.get("/devcamp-Course365/courses",getAllCourse);
router.get("/devcamp-Course365/courses/:courseid",getCourseById);
router.put("/devcamp-Course365/courses/:courseid",updateCourseById);
router.delete("/devcamp-Course365/courses/:courseid",deleteCourseById);

module.exports=router;