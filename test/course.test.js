const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require('../index');
const mongoose = require("mongoose");
const should = chai.should();

chai.use(chaiHttp);

describe("MongoDb connection test", () => {
    it("Should connect to MongoDb", (done) => {
        chai.request(server)
            .get("/test/mongo-connection")
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property("message").eql("MongoDb connected successfully");
                done();
            })
    })
})

describe("POST create course", () => {
    it("Should create a new course", (done) => {
        const newCourse = {
            reqCourseCode:"FE_WEB_RUBYONRAILS_310",
            reqCourseName:"The Complete Ruby on Rails Developer Course",
            reqPrice:2050,
            reqDiscountPrice:1450,
            reqDuration:"8h 30m",
            reqLevel:"Advanced",
            reqCoverImage:"https://media-devcamp.netlify.app/course365/courses/course-rubyonrails.png",
            reqTeacherName:"Claire Robertson",
            reqTeacherPhoto:"https://media-devcamp.netlify.app/course365/teacher/claire_robertson.jpg",
            isPopular:"false",
            isTrending:"true"
        }
        chai.request(server)
            .post('/devcamp-Course365/courses')
            .send(newCourse)
            .end((err, res) => {
                chai.expect(res).to.have.status(201);
                chai.expect(res.body).to.be.a('object');
                chai.expect(res.body).to.have.property('message').eql("Successfully created");
                chai.expect(res.body).to.have.property('result');
                chai.expect(res.body.result).to.have.property("courseCode").eql(newCourse.reqCourseCode);
                chai.expect(res.body.result).to.have.property("courseName").eql(newCourse.reqCourseName);
                chai.expect(res.body.result).to.have.property("price").eql(newCourse.reqPrice);
                chai.expect(res.body.result).to.have.property("discountPrice").eql(newCourse.reqDiscountPrice);
                chai.expect(res.body.result).to.have.property("duration").eql(newCourse.reqDuration);
                chai.expect(res.body.result).to.have.property("level").eql(newCourse.reqLevel);
                chai.expect(res.body.result).to.have.property("coverImage").eql(newCourse.reqCoverImage);
                chai.expect(res.body.result).to.have.property("teacherName").eql(newCourse.reqTeacherName);
                chai.expect(res.body.result).to.have.property("teacherPhoto").eql(newCourse.reqTeacherPhoto);
                done();
            })

    })
})

describe("Get all courses", () => {
    it("It should get all courses of database", (done) => {
        chai.request(server)
            .get("/devcamp-Course365/courses")
            .end((err, res) => {
                res.should.have.status(200);
                done();
            })

    })
})

describe("Get course by Id", () => {
    it("should get a course by valid ID", (done) => {
        const courseid = new mongoose.Types.ObjectId();
        chai.request(server)
            .get(`/devcamp-Course365/courses/${courseid}`)
            .end((err, res) => {
                chai.expect(res).to.have.status(200);
                chai.expect(res.body).to.be.an('object');
                chai.expect(res.body).to.have.property("message").eql("Successfully load data by ID!");
                chai.expect(res.body).to.have.property("data");
                done();
            })
    })
})

describe("Put course by Id", () => {
    it("should update a course by Valid Id", (done) => {
        const courseid = "66293e7247fdc83795c34525";
        const updatCourse = {
            reqPrice:1000,
            reqDiscountPrice:300,
            reqLevel:"Advanced",
            isPopular:"false",
            isTrending:"false"
        }
        chai.request(server)
            .put(`/devcamp-Course365/courses/${courseid}`)
            .send(updatCourse)
            .end((err, res) => {
                try {
                    chai.expect(res).to.have.status(200);
                    chai.expect(res.body).to.be.an("object");
                    chai.expect(res.body).to.have.property("message").eql("Successfully update data by ID!");
                    if (res.body.hasOwnProperty("data") && res.body.data !== null) {
                        const updateCourse = res.body.data;
                        chai.expect(updateCourse).to.have.property("_id").equal(courseid.toString());

                    }
                    else {
                        throw new Error("Response does not contain a valid 'course' property ")
                    }
                    done();
                }
                catch (err) {
                    done(err);
                }
            })
    })
})

describe("Delete course by Id", () => {
    it("should delete a course by valid ID", (done) => {
        const courseid = "66293e7247fdc83795c34525";
        chai.request(server)
            .delete(`/devcamp-Course365/courses/${courseid}`)
            .end((err, res) => {
                try {
                    chai.expect(res).to.have.status(200);
                    chai.expect(res.body).to.be.an('object');
                    done();
                }
                catch (err) {
                    done(err);
                }
            })
    })
})