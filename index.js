//import thu vien express
const express = require('express');

const mongoose=require('mongoose');
const port= 8000;

const app= new express();
app.use(express.json());

mongoose.connect("mongodb://127.0.0.1:27017/Course_NR5")
.then(() =>{
    console.log("Connect mongoDB Successfully");
})
.catch((error) =>{
    console.log(error);
});

app.get("/test/mongo-connection",(req,res)=>{
    res.status(200).json({
        message:"MongoDb connected successfully"
    })
})

const courseRouter=require("./app/router/courseRouter");

app.use("/",courseRouter);

app.listen(port, () => {
    console.log(`App  listening  on port ${port}`);
})

module.exports=app;